About
-----

This module disables Drupal default front page (/node) menu hook if a website's
front page is not configured to use it.

According to Production check & Production monitor module security tests, having /node
page avaiable while it's not used considered as a security risk:

  "The default /node page created by Drupal core is still enabled. With improper setup 
   of node types, this can reveal sensitive information (e.g. using the profile module 
   with automatic publish to front activated)!"
